import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LoadingController, ModalController} from '@ionic/angular';
import {MassiveSearchModalComponent} from '../massive-search-modal/massive-search-modal.component';
import {CccInputTextComponent} from '../../webcomponents/ccc-input-text/ccc-input-text.component';

@Component({
    selector: 'app-search-form',
    templateUrl: './search-form.component.html',
    styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent implements OnInit {
    @Output() messageEvent = new EventEmitter<any>();
    public objectKeys = Object.keys;
    public filterForm: FormGroup;
    public advanceForm = false;
    public orderNumber: any;
    public rut: any;
    public origins = {
        falabella: 'Falabella',
        sodimac: 'Sodimac',
        linio: 'linio',
        tottus: 'Tottus'
    };

    constructor(public loadingController: LoadingController, public modalController: ModalController) {
        this.renderForm();
    }

    ngOnInit = () => {
    };
    showAdvance = () => {
        this.advanceForm = (!this.advanceForm);
    };
    renderForm = (): void => {
        this.filterForm = new FormGroup({
            orderNumber: new FormControl(''),
            rut: new FormControl(''),
            advance: new FormGroup({
                originBusiness: new FormControl(''),
                destinyBusiness: new FormControl(''),
                withdrawalPoint: new FormControl(''),
                buyDate: new FormControl(''),
                withdrawalDate: new FormControl(''),
                orderStatus: new FormControl(''),
                invoiceNumber: new FormControl('')
            })
        });
    }
    /**
     * @description invoke modal to search massive orders
     * @return Promise
     */
    massiveSearchModal = async () => {
        const modal = await this.modalController.create({
            component: MassiveSearchModalComponent,
            backdropDismiss: false,
        });
        await modal.present();
        const {data} = await modal.onDidDismiss();
        this.messageEvent.emit(data);
    }
    /**
     * @description search function to reactive trazability form
     */
    search = async () => {
        this.filterForm.value.orderNumber = this.orderNumber;
        this.filterForm.value.rut = this.rut;
        console.log(this.filterForm.value);
        const loading = await this.loadingController.create({
            message: 'Searching...',
            duration: 3000
        });
        await loading.present();
        this.messageEvent.emit(this.filterForm.value);
    }
}
