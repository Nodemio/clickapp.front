import {Component, OnInit} from '@angular/core';
import {NavParams, ModalController} from '@ionic/angular';

@Component({
    selector: 'app-successfull-reception-modal',
    templateUrl: './successfull-reception-modal.component.html',
    styleUrls: ['./successfull-reception-modal.component.scss'],
})
export class SuccessfullReceptionModalComponent implements OnInit {
    // public orderList: any;

    constructor(
        public navParams: NavParams,
        public modalController: ModalController
    ) {
        // console.log(navParams.get('prop1'));
    }

    ngOnInit() {
    }

    finish = () => {
        this.modalController.dismiss({
            // result: this.orderList
        });
    };
}
