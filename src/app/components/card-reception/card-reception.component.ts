import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'card-reception',
  templateUrl: './card-reception.component.html',
  styleUrls: ['./card-reception.component.scss'],
})
export class CardReceptionComponent implements OnInit {

  @Input('title') title: string;
  @Input('className') className: string;
  @Input('list') list: [any];
  @Input() model: any;
  @Input() type: string;
  @Input() observations: boolean;
  public validatedObservations: boolean;
  @Input() disabled: boolean;

  @Output() modelChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.validatedObservations = typeof this.observations === 'boolean' ? this.observations : false;
    console.log('validatedObservations: ', this.validatedObservations);
  }

  sendInput() {
    this.modelChange.emit(this.model);
    this.model = '';
  }
}
