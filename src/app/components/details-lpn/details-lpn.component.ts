// @ts-ignore
import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-details-lpn',
  templateUrl: './details-lpn.component.html',
  styleUrls: ['./details-lpn.component.scss'],
})
export class DetailsLpnComponent implements OnInit {
  public headersTable = [
    {
      name: 'Description'
    },
    {
      name: 'SKU'
    },
    {
      name: 'Quantity'
    }
  ];
  public listProducts = [
    {
      description: 'Iphone',
      sku: '112FDSS',
      quantity: 1
    },
    {
      description: 'MacBook Pro',
      sku: 'RRTT33',
      quantity: 2
    }
  ];

  constructor(public modalController: ModalController) { }

  ngOnInit() {}
  close = () => {
    this.modalController.dismiss({
      result: false
    });
  }

}
