import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-massive-search-modal',
  templateUrl: './massive-search-modal.component.html',
  styleUrls: ['./massive-search-modal.component.scss'],
})
export class MassiveSearchModalComponent implements OnInit {
  public orderList: any;

  constructor(public navParams: NavParams, public modalController: ModalController) {
    console.log(navParams.get('prop1'));
  }

  ngOnInit() {}
  finish = () => {
    this.modalController.dismiss({
      result: this.orderList
    });
  }
  cancel = () => {
    this.modalController.dismiss({
      result: false
    });
  }

}
