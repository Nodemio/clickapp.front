import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams} from '@ionic/angular';

@Component({
  selector: 'app-document-reception-modal',
  templateUrl: './document-reception-modal.component.html',
  styleUrls: ['./document-reception-modal.component.scss'],
})
export class DocumentReceptionModalComponent implements OnInit {
  // public orderList: any;

  constructor(
    public navParams: NavParams, 
    public modalController: ModalController
  ) { 
    // console.log(navParams.get('prop1'));
  }

  ngOnInit() {
  }

  finish = () => {
    this.modalController.dismiss({
      // result: this.orderList
    });
  }
}
