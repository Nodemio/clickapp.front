import {Component, OnInit} from '@angular/core';
import {DetailsLpnComponent} from '../details-lpn/details-lpn.component';
import {ModalController} from '@ionic/angular';


@Component({
    selector: 'app-orders-table',
    templateUrl: './orders-table.component.html',
    styleUrls: ['./orders-table.component.scss']
})
export class OrdersTableComponent implements OnInit {


    constructor(public modalController: ModalController) {

    }

    ngOnInit() {
    }

    viewDetails = async () => {
        const modal = await this.modalController.create({
            component: DetailsLpnComponent,
            backdropDismiss: false,
            componentProps: {
                prop1: 'value',
                prop2: 'value2'
            }
        });
        await modal.present();
        const {data} = await modal.onDidDismiss();
        console.log(data);
    }

}
