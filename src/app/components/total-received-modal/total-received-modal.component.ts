import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-total-received-modal',
  templateUrl: './total-received-modal.component.html',
  styleUrls: ['./total-received-modal.component.scss'],
})
export class TotalReceivedModalComponent implements OnInit {
  // public orderList: any;

  @Output() confirmButtonCallback: EventEmitter<any> = new EventEmitter();


  constructor(
    public navParams: NavParams, 
    public modalController: ModalController
  ) { 
    // console.log('CALLBACK: ', navParams.get('confirmButtonCallback'));
  }

  ngOnInit() {
    console.log(this.navParams);
  }

  finish = () => {
    this.modalController.dismiss({
      callConfirm: false
    });
  }

  confirm = () => {
    this.modalController.dismiss({
      callConfirm: true
    });
    // this.confirmButtonCallback.emit();
    // console.log('CALLBACK: ', this.navParams.get('confirmButtonCallback'));
    // this.navParams.get('confirmButtonCallback');
  }
}
