import { Injectable } from '@angular/core';

@Injectable()
export class MathService {

  constructor() { }
  sum = (...args: any[]) => {
    return args.reduce((a: any, b: any) => {
      return a + b;
    });
  }
}
