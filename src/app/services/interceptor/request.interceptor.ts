import {Injectable} from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                (event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        console.log(event);
                    }
                },
                (err: any) => {
                    if (err instanceof HttpErrorResponse) {
                        switch (err.status) {
                            case 404:
                                console.log('Not found page!');
                                break;
                            case 500:
                                console.log('Error papu :c');
                                break;
                            default:
                                console.log('Error Request (' + err.status + '): ' + err.statusText);
                                break;
                        }
                    }
                })
        );
    }
}
