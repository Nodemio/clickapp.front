import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface IUser {
  userId: string;
  business: string;
  receptionShop: string;
}

@Injectable()
export class UserService {

  public userSubject = new BehaviorSubject<IUser>({
    userId: '18.932.192-4',
    business: 'Falabella',
    receptionShop: 'Falabella Costanera Center',
  });
  public userSubject$ = this.userSubject.asObservable();

  constructor() { }

  unsubscribe(): void {
    this.userSubject.complete();
  }

}
