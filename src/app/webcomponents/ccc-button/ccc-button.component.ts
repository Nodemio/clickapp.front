import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';

@Component({
  selector: 'ccc-button',
  templateUrl: './ccc-button.component.html',
  styleUrls: ['./ccc-button.component.scss'],
})
export class CccButtonComponent implements OnInit {

  @Input() title: string;
  @Input() icon: string;
  @Input() type: string;
  // @Input() block: boolean;
  validatedIcon: string;
  @Input() disabled: boolean;

  @Output() buttonClick: EventEmitter<any> = new EventEmitter();

  // @HostBinding('class') @Input('class') classList: string = 'MANFREDO';

  constructor() { }

  ngOnInit() {
    // console.log("block: ", this.block);
    this.validatedIcon = typeof this.icon === 'string' && this.icon.length > 0 ? this.icon : '';
    // console.log("validatedBlock: ", this.validatedBlock);
  }

  click(): void {
    this.buttonClick.emit();
  }

}
