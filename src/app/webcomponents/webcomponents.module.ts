import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CccInputTextComponent} from './ccc-input-text/ccc-input-text.component';
import {CccButtonComponent} from './ccc-button/ccc-button.component';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';


@NgModule({
  declarations: [CccInputTextComponent, CccButtonComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [CccInputTextComponent, CccButtonComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WebcomponentsModule { }
