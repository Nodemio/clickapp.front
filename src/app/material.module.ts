import { NgModule } from '@angular/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {SatDatepickerModule, SatNativeDateModule} from 'saturn-datepicker';

import {
    MatTableModule,
    MatStepperModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatNativeDateModule
} from '@angular/material';

@NgModule({
    imports: [
        MatDatepickerModule,
        MatNativeDateModule,
        SatDatepickerModule,
        SatNativeDateModule
    ],
    exports: [
        MatTableModule,
        MatStepperModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatOptionModule,
        MatSelectModule,
        MatPaginatorModule,
        MatSortModule,
        MatDatepickerModule,
        SatDatepickerModule,
        SatNativeDateModule
    ],
    providers: [
        MatDatepickerModule
    ],
})
export class MaterialModule {}
