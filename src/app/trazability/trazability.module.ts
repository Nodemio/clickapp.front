import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {IonicModule} from '@ionic/angular';

import {TrazabilityPage} from './trazability.page';
import {ApiService} from '../services/api.service';
import {MassiveSearchModalComponent} from '../components/massive-search-modal/massive-search-modal.component';
import {SearchFormComponent} from '../components/search-form/search-form.component';
import {OrdersService} from './utils/orders.service';
import {DetailsLpnComponent} from '../components/details-lpn/details-lpn.component';
import {OrdersTableComponent} from '../components/orders-table/orders-table.component';

import {MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatSelectModule} from '@angular/material';
import {MaterialModule} from '../material.module';
import {WebcomponentsModule} from '../webcomponents/webcomponents.module';

const routes: Routes = [
    {
        path: '',
        component: TrazabilityPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule.forChild(routes),
        MaterialModule,
        WebcomponentsModule
    ],
    declarations: [
        TrazabilityPage,
        SearchFormComponent,
        MassiveSearchModalComponent,
        DetailsLpnComponent,
        OrdersTableComponent
    ],
    entryComponents: [
        SearchFormComponent,
        MassiveSearchModalComponent,
        DetailsLpnComponent,
        OrdersTableComponent
    ],
    providers: [ApiService, OrdersService, MatDatepickerModule],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrazabilityPageModule {
}
