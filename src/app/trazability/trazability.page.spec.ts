import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrazabilityPage } from './trazability.page';

describe('TrazabilityPage', () => {
  let component: TrazabilityPage;
  let fixture: ComponentFixture<TrazabilityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrazabilityPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrazabilityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
