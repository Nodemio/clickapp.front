import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {FormGroup} from '@angular/forms';
import {OrdersService} from './utils/orders.service';
import {MenuController, ModalController} from '@ionic/angular';


@Component({
    selector: 'app-trazability',
    templateUrl: './trazability.page.html',
    styleUrls: ['./trazability.page.scss'],
})
export class TrazabilityPage implements OnInit {
    public title: any = 'Hi trazability';
    public form: FormGroup;
    public buffer: any;
    public orders: any;
    public variable: any;
    public element: any;
    public currentLpn: any = {
        bundleSize: 2,
        products: [
            {
                lpnId: '13322',
                description: 'Mesa de Terraza',
                state: 'En Ruta',
                qt: 1,
                details: [
                    {
                        description: 'Sillas',
                        quantity: 4,
                        sku: '10004556'
                    },
                    {
                        description: 'Mesa',
                        quantity: 1,
                        sku: '10007777'
                    }
                ]
            },
            {
                lpnId: '134444',
                description: '',
                state: 'Cancelado',
                qt: 1,
                details: [
                    {
                        description: 'Sillas',
                        quantity: 4,
                        sku: '10004556'
                    }
                ]
            }
        ]

    };
    public keys: any = Object.keys;

    constructor(public api: ApiService,
                public orderService: OrdersService,
                public modalController: ModalController,
                public menu: MenuController) {
        this.title = 'Trazabilidad del Cross Dock';
        this.orders = [
            {
                number: '12345678',
                subOrder: '123456',
                userId: 'anything',
                thirdId: '1001',
                originBusiness: 'FALABELLA',
                retirementPoint: 'SODIMAC',
                dateCreation: '18-06-2019',
                dateRetirement: '20-06-2019',
                ocState: 'Recepcion'

            },
            {
                number: '12345679',
                subOrder: '123456',
                userId: 'anything',
                thirdId: '1001',
                originBusiness: 'FALABELLA',
                retirementPoint: 'SODIMAC',
                dateCreation: '18-06-2019',
                dateRetirement: '20-06-2019',
                ocState: 'Recepcion'

            }
        ];
    }

    ngOnInit = (): void => {
    };
    /**
     * @description receive form from child component
     * @param $event Validated form
     */
    receiveForm = ($event: any): void => {
        this.form = $event;
        this.orderService.getOrder(this.form)
            .then((data: any) => {
                this.orders = data;
            })
            .catch(() => {
                console.log('error consume trazability page');
            });
    };

    details(id: any) {
        if (this.buffer === id) {
            this.buffer = false;
        } else {
            this.buffer = id;
        }
        this.orderService.getLpn(id)
            .then((data: any) => {
                this.currentLpn = data;
            })
            .catch(() => {
                console.log('error consume trazability page');
            });
    }
}
