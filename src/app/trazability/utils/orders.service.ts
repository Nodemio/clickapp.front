import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class OrdersService {
    public response;
    constructor(public http: HttpClient) {
    }

    getOrder = async (orders: object | object[]) => {
        const url = 'https://bff.falabella.io/v1/trazability/search';
        await this.http.post(url, orders)
            .subscribe(
                (res: any) => {
                    this.response = res.description;
                },
                (err: any) => {
                    console.log(err, 'error from consume trazability to order list');
                },
                () => {
                    console.log('complete consume data from trazability to order list');
                });
        return this.response;
    }

    getLpn = async (orderId: any) => {
        const url = 'https://bff.falabella.io/v1/customer-orders/lpn/search-store/' + orderId;
        await this.http.get(url)
            .subscribe(
                (res: any) => {
                    this.response = res;
                },
                (err: any) => {
                    console.log(err, 'error from consume trazability to lpn');
                },
                () => {
                    console.log('complete consume data from trazability to lpn');
                });
        return this.response;
    }
}
