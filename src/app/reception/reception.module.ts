import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {HttpClientModule} from '@angular/common/http';
import {IonicModule} from '@ionic/angular';

import {ReceptionPage} from './reception.page';
import {UserService} from '../services/user/user.service';
import {LpnsService} from './utils/lpns.service';

import {CardReceptionComponent} from '../components/card-reception/card-reception.component';
import {CccButtonComponent} from '../webcomponents/ccc-button/ccc-button.component';

import {DocumentReceptionModalComponent} from '../components/document-reception-modal/document-reception-modal.component';
import {TotalReceivedModalComponent} from '../components/total-received-modal/total-received-modal.component';
import {SuccessfullReceptionModalComponent} from '../components/successfull-reception-modal/successfull-reception-modal.component';
import {AutofocusDirective} from './auto-focus.directive';
import {WebcomponentsModule} from '../webcomponents/webcomponents.module';

const routes: Routes = [
    {
        path: '',
        component: ReceptionPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        HttpClientModule,
        WebcomponentsModule
    ],
    declarations: [
        ReceptionPage,
        CardReceptionComponent,
        DocumentReceptionModalComponent,
        TotalReceivedModalComponent,
        SuccessfullReceptionModalComponent,
        AutofocusDirective
    ],
    entryComponents: [
        DocumentReceptionModalComponent,
        TotalReceivedModalComponent,
        SuccessfullReceptionModalComponent,
        CardReceptionComponent
    ],
    providers: [
        UserService,
        LpnsService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class ReceptionPageModule {
}
