import {
    IListLpns,
    ILpnItem,
 } from './lpns.service';

export function checkStr(str: any, length: any) {
    if (str.length === 0) {
        return {
          lpnId: str,
          check: false,
        };
    } else {
      const strTrim = (arg: any) => arg.trim();
      const strTrimmed = strTrim(str);
      if (strTrimmed.length === length && !/\s/.test(strTrimmed)) {
        return {
          lpnId: strTrimmed,
          check: true,
        };
      } else {
        return {
          lpnId: strTrimmed,
          check: false,
        };
      }
    }
}
