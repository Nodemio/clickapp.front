import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { LoadingController } from '@ionic/angular';

import {
  HttpClient,
} from '@angular/common/http';

declare var require: any;
const orderLpns = require('./dataLpnsDummy.json');
import {
  checkStr
} from './lpns.utils';

export interface ILpnTrace {
  traceStatus: string;
  site: string;
  plannedDate: string;
  receptionDate: string;
  user: string;
  carrier: string;
  licensePlate: string;
  originFacilityIdXD: string;
  originFacilityNameXD: string;
  destinationFacilityIdXD: string;
  destinationFacilityNameXD: string;
}

export interface ILpn {
  lpnId: string;
  lpnStatus: string;
  sku: string;
  skuDescription: string;
  quantity: string;
  receptionDocumentType: string;
  lpnStatusDate: string;
  totalTraces: string;
  firstOriginFacilityId: string;
  firstOriginFacilityName: string;
  lastDestinationFacilityId: string;
  lastDdestinationFacilityName: string;
  lpnTrace: ILpnTrace[],
}

export interface IOrderLpn {
  orderId: string;
  lpn: ILpn[];
}

export interface ILpnItem {
  lpnId: string;
  status: string;
  observation: string;
  originFacilityNameXD: string;
}
export interface IListLpns {
  lpnsForRecieving: ILpnItem[];
  lpnsReceived: ILpnItem[];
  lpnsWithObservations: ILpnItem[];
}

export interface IStoreReceptionDisabled {
  receptionDocumentDisabled: boolean;
  lpnReadDisabled: boolean;
  confirmReceptionDisabled: boolean;
}

@Injectable()
export class LpnsService {

  public lpns: IOrderLpn;
  private listLpnsSubject = new BehaviorSubject<IListLpns>({
    lpnsForRecieving: [],
    lpnsReceived: [],
    lpnsWithObservations: []
  });
  public listLpnsSubject$ = this.listLpnsSubject.asObservable();


  private storeReceptionDisabledSubject = new BehaviorSubject<IStoreReceptionDisabled>({
    receptionDocumentDisabled: false, // false,
    lpnReadDisabled: true,
    confirmReceptionDisabled: true
  });
  public storeReceptionDisabledSubject$ = this.storeReceptionDisabledSubject.asObservable();


  constructor(
    private http: HttpClient,
    public loadingController: LoadingController
  ) {
    this.lpns = orderLpns;
  }

  async getLpns(gdd: any): Promise<any> {

    //const checkGddResponse: any = checkStr(gdd, 5);

    if (gdd > 0) {
      const loading = await this.loadingController.create({
        message: 'Espere, por favor',
      });
      await loading.present();
      const url = 'https://bff.falabella.io/v1/customer-orders/lpn/reception-doc-type/321'; // OK
      this.http.get(url).subscribe((res: IOrderLpn) => {
        const response: IListLpns = {
          lpnsForRecieving: Array
            .from(new Set(res.lpn))
            .map(lpn => ({
              lpnId: lpn.lpnId,
              status: 'forRecieving',
              observation: '',
              originFacilityNameXD: lpn.lpnTrace[0].originFacilityNameXD,
            })),
          lpnsReceived: [],
          lpnsWithObservations: [],
        };
        this.listLpnsSubject.next(response);

        const storeReceptionDisabledResponse: IStoreReceptionDisabled = {
          receptionDocumentDisabled: true,
          lpnReadDisabled: false,
          confirmReceptionDisabled: false
        }
        this.storeReceptionDisabledSubject.next(storeReceptionDisabledResponse);

        loading.dismiss();
      });
    }

  }

  async receiveLpn(lpnId: any, receptionShop: any): Promise<any> {
    const listLpnsEdit: IListLpns = Object.assign({}, this.listLpnsSubject.value);
    const checkLpnResponse: any = checkStr(lpnId, 18);

    if (checkLpnResponse.check) {
      
      const lpnInForRecieving: ILpnItem = listLpnsEdit
        .lpnsForRecieving
        .find(lpn => lpn.lpnId === checkLpnResponse.lpnId);

      let response: IListLpns;

      if (lpnInForRecieving) {

        const lpnsForRecievingEdit: ILpnItem[] = listLpnsEdit
          .lpnsForRecieving
          .filter(lpn => lpn.lpnId !== checkLpnResponse.lpnId);

        if (lpnInForRecieving.originFacilityNameXD === receptionShop) {
          response = {
            lpnsForRecieving: lpnsForRecievingEdit,
            lpnsReceived: [
              ...listLpnsEdit.lpnsReceived,
              {
                ...lpnInForRecieving,
                status: 'received',
                observation: '',
              },
            ],
            lpnsWithObservations: [
              ...listLpnsEdit.lpnsWithObservations,
            ],
          };
        } else {
          response = {
            lpnsForRecieving: lpnsForRecievingEdit,
            lpnsReceived: [
              ...listLpnsEdit.lpnsReceived,
            ],
            lpnsWithObservations: [
              ...listLpnsEdit.lpnsWithObservations,
              {
                ...lpnInForRecieving,
                status: 'whitObservation',
                observation: 'Tienda errònea',
              },
            ],
          };
        }
        this.listLpnsSubject.next(response);

      } else {

        const lpnInReceived: boolean = listLpnsEdit
          .lpnsReceived
          .some(lpn => lpn.lpnId === checkLpnResponse.lpnId);

        const lpnInWithObservations: boolean = listLpnsEdit
          .lpnsWithObservations
          .some(lpn => lpn.lpnId === checkLpnResponse.lpnId);

        if (lpnInReceived || lpnInWithObservations) {
          return true;
        } else {
          const loading = await this.loadingController.create({
            message: 'espere porfavor',
          });
          await loading.present();
          const url = `https://bff.falabella.io/v1/customer-orders/lpn/search-store/${checkLpnResponse.lpnId}`; // OK
          this.http.get(url).subscribe((res: any) => {

            console.log('RespuestaShear', res.hasLpnStore);

            if (res.hasLpnStore) {
              response = {
                lpnsForRecieving: [
                  ...listLpnsEdit.lpnsForRecieving,
                ],
                lpnsReceived: [
                  ...listLpnsEdit.lpnsReceived,
                  {
                    lpnId: checkLpnResponse.lpnId,
                    originFacilityNameXD: receptionShop,
                    status: 'received',
                    observation: 'surplus',
                  },
                ],
                lpnsWithObservations: [
                  ...listLpnsEdit.lpnsWithObservations,
                ],
              };
            } else {
              response = {
                lpnsForRecieving: [
                  ...listLpnsEdit.lpnsForRecieving,
                ],
                lpnsReceived: [
                  ...listLpnsEdit.lpnsReceived,
                ],
                lpnsWithObservations: [
                  ...listLpnsEdit.lpnsWithObservations,
                  {
                    lpnId: checkLpnResponse.lpnId,
                    originFacilityNameXD: '',
                    status: 'whitObservation',
                    observation: 'Tienda errònea',
                  },
                ],
              };
            }
            this.listLpnsSubject.next(response);
            loading.dismiss();
            });
        }

      }

    }
  }

  unsubscribe(): void {
    this.listLpnsSubject.complete();
    this.storeReceptionDisabledSubject.complete();
  }

  async cancel(): Promise<any> {

    const loading = await this.loadingController.create({
      message: 'Espere, por favor',
    });

    await loading.present();

    this.listLpnsSubject.next({
      lpnsForRecieving: [],
      lpnsReceived: [],
      lpnsWithObservations: [],
    });

    const storeReceptionDisabledResponse: IStoreReceptionDisabled = {
      receptionDocumentDisabled: false,
      lpnReadDisabled: true,
      confirmReceptionDisabled: true
    }
    this.storeReceptionDisabledSubject.next(storeReceptionDisabledResponse);

    loading.dismiss();
  }

  async confirm(cb: any): Promise<any> {

    const loading = await this.loadingController.create({
      message: 'Espere, por favor',
    });
    await loading.present();
    const url = 'https://bff.falabella.io/v1/customer-orders/lpn/reception-doc-type/321'; // OK
    this.http.get(url).subscribe((res: IOrderLpn) => {
      const response: IListLpns = {
        lpnsForRecieving: [],
        lpnsReceived: [],
        lpnsWithObservations: [],
      };
      loading.dismiss();

      cb();
      this.listLpnsSubject.next(response);

      const storeReceptionDisabledResponse: IStoreReceptionDisabled = {
        receptionDocumentDisabled: false,
        lpnReadDisabled: true,
        confirmReceptionDisabled: true
      }
      this.storeReceptionDisabledSubject.next(storeReceptionDisabledResponse);

    });



  }

}
