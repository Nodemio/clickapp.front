import { TestBed } from '@angular/core/testing';

import { LpnsService } from './lpns.service';

describe('LpnsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LpnsService = TestBed.get(LpnsService);
    expect(service).toBeTruthy();
  });
});
