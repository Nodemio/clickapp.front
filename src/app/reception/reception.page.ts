import {
    Component,
    OnInit,
} from '@angular/core';

import {
    ModalController,
    LoadingController
} from '@ionic/angular';
import {OverlayEventDetail} from '@ionic/core';

import {
    IListLpns,
    LpnsService,
    IOrderLpn,
    IStoreReceptionDisabled
} from './utils/lpns.service';

import {
    IUser,
    UserService
} from '../services/user/user.service';

import {DocumentReceptionModalComponent} from '../components/document-reception-modal/document-reception-modal.component';
import {TotalReceivedModalComponent} from '../components/total-received-modal/total-received-modal.component';
import {SuccessfullReceptionModalComponent} from '../components/successfull-reception-modal/successfull-reception-modal.component';


@Component({
    selector: 'app-reception',
    templateUrl: './reception.page.html',
    styleUrls: ['./reception.page.scss'],
})
export class ReceptionPage implements OnInit {
    public gdd: any;
    public lpnId: any;
    public user: IUser;
    public listLpns: IListLpns;
    public storeReceptionDisabled: IStoreReceptionDisabled;

    public posts: IOrderLpn;

    constructor(
        public lpnsService: LpnsService,
        public userService: UserService,
        public modalController: ModalController,
        public loadingController: LoadingController
    ) {
        this.gdd = '';
        this.lpnId = '';
    }

    ngOnInit() {
        this.lpnsService.listLpnsSubject$.subscribe(listLpns => {
            this.listLpns = listLpns;
        });

        this.userService.userSubject$.subscribe(user => {
            this.user = user;
        });

        this.lpnsService.storeReceptionDisabledSubject$.subscribe(storeReceptionDisabled => {
            this.storeReceptionDisabled = storeReceptionDisabled;
        });
    }

    async getLpnForGdd() {
        this.lpnsService.getLpns(this.gdd);
    }

    checkLpn() {
        this.lpnsService.receiveLpn(this.lpnId, this.user.receptionShop);
        this.lpnId = '';
    }

    modelChanged(e: any) {
        this.lpnId = e;
        this.checkLpn();
    }

    inputDocumentReceptionModal = async () => {
        const modal = await this.modalController.create({
            component: DocumentReceptionModalComponent,
            componentProps: {
                prop1: 'value',
                prop2: 'value2',
            },
            cssClass: 'modal-ccc-type-1 modal-height-document-reception',
            backdropDismiss: false
        });

        await modal.present();
        const {data} = await modal.onDidDismiss();
        // console.log(data);
    }

    openTotalReceivedModal = async () => {
        const modal = await this.modalController.create({
            component: TotalReceivedModalComponent,
            componentProps: {
                received: (this.listLpns.lpnsReceived.length + this.listLpns.lpnsWithObservations.length),
                total: (this.listLpns.lpnsForRecieving.length + this.listLpns.lpnsReceived.length
                    + this.listLpns.lpnsWithObservations.length),
                surplus: (this.listLpns.lpnsReceived.filter(lpn => lpn.observation === 'surplus').length),
                withObservations: (this.listLpns.lpnsWithObservations.length),
                missing: (this.listLpns.lpnsForRecieving.length),
                confirmButtonCallback: this.confirmTotalReceivedModal,
            },
            cssClass: 'modal-ccc-type-2 footer-bar modal-height-total-received',
            backdropDismiss: false
        });

        modal.onDidDismiss().then(async (detail: OverlayEventDetail) => {
            // console.log("HOLA MANFRED");
            /* if (detail !== null) {
              console.log('The result:', detail.data);
            } */
            // console.log("CERRO EL MODAL con: ", detail.data.callConfirm);

            if (detail.data.callConfirm) {
                this.lpnId = null;
                this.lpnsService.confirm(this.confirmTotalReceivedModal);
                this.gdd = '';
            }
        });

        await modal.present();
        // const { data } = await modal.onDidDismiss()
        // console.log(data);
    }

    confirmTotalReceivedModal = () => {
        // LOADING
        this.openSuccessfulReceptionModal();
    }

    openSuccessfulReceptionModal = async () => {
        const modal = await this.modalController.create({
            component: SuccessfullReceptionModalComponent,
            componentProps: {
                total: (this.listLpns.lpnsForRecieving.length + this.listLpns.lpnsReceived.length
                    + this.listLpns.lpnsWithObservations.length),
                surplus: (this.listLpns.lpnsReceived.filter(lpn => lpn.observation === 'surplus').length),
            },
            cssClass: 'modal-ccc-type-1 footer-bar modal-height-successful-reception',
            backdropDismiss: false
        });
        await modal.present();
        const {data} = await modal.onDidDismiss();
        // console.log(data);
    }

    cancel() {
        this.gdd = '';
        this.lpnsService.cancel();
    }

    confirmReception() {
        this.gdd = '';
        this.lpnsService.cancel();
    }
}
