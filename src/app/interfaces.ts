export interface TableCols {
    ordenCompra: number;
    subOrden: number;
    rutCliente: string;
    terceroHabilitado: string;
    negocioOrigen: string;
    puntoRetiro: string;
    fechaCompra: string;
    fechaRetiro: string;
    estadoOC: string;
}
